from modules.imported import *

class UX: 
    def createUX (self):
        UX.bodyFrame(self)
        UX.menuBar(self)
        UX.welcomeFrame(self)
        #UX.logoFrame(self)
        UX.infoFrame(self)
        UX.buttonFrame(self)

    def bodyFrame (self):
        self.body = Tk()
        self.body.configure(bg='white')
        self.body.title('Selscrape')
        self.body.maxsize(width=900, height=800)

    def menuBar (self):
        self.menubar = Menu(self.body)
        self.menubar.add_command(label="Quit", command=self.body.quit)
        self.body.configure(menu=self.menubar)
        self.logo = PhotoImage(file='art/Black.gif')

    def welcomeFrame (self):
        self.welcomeFrame = Frame(self.body)
        self.welcomeTag = Label(self.welcomeFrame, text = 'Automated Web Scraper').pack(side=LEFT,fill=BOTH)

    def logoFrame (self):
        self.logoFrame = Frame(self.body)
        self.logoLab = Label(self.logoFrame,image=self.logo)
        self.logoLab.image = self.logo
        self.logoLab.pack(padx=30, side=LEFT)
        self.logoFrame.grid(row=0,column=0)
        self.totalFrame = Frame(self.body, bg = 'black', bd=1, relief=SUNKEN)
        self.v = IntVar()
        self.v.set("0")
        self.status = StringVar()
        self.status.set('Select an Option')
        self.totalLal = Label(self.totalFrame, textvariable = self.v, font=('San Francisco', 78), bg = 'black', fg='green', wraplength=400).pack(padx=10,fill=BOTH)
        staticstat = Label(self.totalFrame, text="Current Status: ", font=('San Francisco', 28), bg = 'black', fg='blue', wraplength=400 ).pack(padx=10,fill=BOTH)
        self.statusLab = Label(self.totalFrame, textvariable = self.status, font=('San Francisco', 28), bg = 'black', fg='red', wraplength=400).pack(padx=10,fill=BOTH)
        self.totalFrame.grid(row=0,column=1)

    def infoFrame (self):
        self.status = StringVar()
        self.status.set('Select an Option')
        self.infoFrame = Frame(self.body,bd=1, relief=SUNKEN)
        self.checktime = StringVar()
        self.newEtsyOrd = IntVar()
        self.newAmOrd = IntVar()
        self.tt = StringVar()
        self.newEtsyOrd.set(0)
        self.newAmOrd.set(0)
        self.tt.set('-:--')
        self.checktime.set('-:--')
        staticstat = Label(self.infoFrame, text="Current Status: ", font=('San Francisco', 20)).grid(sticky='w', row=0, column=0)
        self.statusLab = Label(self.infoFrame, textvariable = self.status, font=('San Francisco', 20), fg='red').grid(sticky='w', row=0, column=1)
        self.etsylab = Label(self.infoFrame, text = 'New Etsy Orders: ', font=('San Francisco', 20)).grid(sticky='w',row=1,column=0)
        self.newetsyorders = Label(self.infoFrame, textvariable=self.newEtsyOrd, font=('San Francisco', 20)).grid(sticky='w', row=1, column=1)
        self.amlab = Label(self.infoFrame, text = 'New Amazon Orders: ', font=('San Francisco', 20)).grid(sticky='w',row=2,column=0)      
        self.newamorders = Label(self.infoFrame, textvariable=self.newAmOrd, font=('San Francisco', 20)).grid(sticky='w', row=2,column=1)
        self.lastchecklab = Label(self.infoFrame, text = 'Last Order Check: ', font=('San Francisco', 20)).grid(sticky='w',row=3,column=0)
        self.lastCheck = Label(self.infoFrame, textvariable=self.checktime, font=('San Francisco', 20)).grid(sticky='w', row=3, column=1)
        self.lastcsvlab = Label(self.infoFrame, text = 'Last Counter Reset: ', font=('San Francisco', 20)).grid(sticky='w',row=4,column=0)
        self.lastCSV = Label(self.infoFrame, textvariable=self.tt, font=('San Francisco', 20)).grid(sticky='w', row=4, column=1)
        self.infoFrame.grid(padx=10,pady=10,row=5, column=1)

    def buttonFrame (self):
        self.orderCheckFrame = Frame(self.body, bd=1, relief=SUNKEN)
        self.clear = Button(self.orderCheckFrame, command=self.warning, text='Clear Counters', width=25)
        self.check = Button(self.orderCheckFrame, command=self.startgetNewOrders, text='Check for New Orders',width=25)
        self.automate = Button(self.orderCheckFrame, command=self.startSchedule, text='Start Schedule',width=25)
        self.browse = Button(self.orderCheckFrame, command=self.openBrowser, text='Open Browser',width=25)
        self.l = Label(self.orderCheckFrame, text="Select an Option Below:", font=('San Francisco', 20))
        self.l.grid(sticky='w',row=0)
        self.browse.grid(sticky='w', row=1)
        self.check.grid(sticky='w',row=2)
        self.automate.grid(sticky="w", row=3)
        self.clear.grid(sticky='w',row=4)
        self.quitbutton = Button(self.orderCheckFrame,command=self.quitAll, text='Quit', width=25).grid(sticky='w',row=5)
        self.orderCheckFrame.grid(padx=10,pady=10,row=5, column=0)