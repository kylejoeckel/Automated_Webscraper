from modules.imported import *

class Amazon:
    dateActual = time.strftime('%c')
    now = time.strftime("%m-%d-%Y")
    def login(self):
        self.status.set("Logging In - Amazon")
        totp = pyotp.TOTP("4DBGALAT52K5RAKZ3FFJMSIB3GMIIAJDFX6TBO6EY3JN2RSI4DGA")
        code = totp.now()
        url = "https://sellercentral.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fsellercentral.amazon.com%2Fgp%2Fhomepage.html&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=sc_na_amazon_v2&_encoding=UTF8&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&language=en_US&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=sc_na_amazon&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&ssoResponse=eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.3SLtHsoWa9A_rDLRyVWMrtRwIAKnBMx1c9IyL6-8hyYtRuaaj7gjTA.3keimbKYWhxhF1Im.PcAln7EFfci_z-glB9PDpA3Z-5f37-gjrJYNosu5Bwp_awSkjfVf6xMhX3m6zzP82fdrAoW04Eivjx973bXBcT5SKxhH1-cga6eBu8E10CGUeYPtC-2Wk9goDFFA-IKN51T-1PNhBWFgAQhkgVe6wnUIEfDiEq4UbZoVnTLTd1NSxW8Dh7B7wiM0FBJGfPeWb_BJanbSWdlcqhNB5E990S5cIMkz__ycZvjdyWx3D2Bc3rxpMT5jwNrLFi65lshqAz97Hqq18fjYiLEI-5TAZBitXoX6y7A9Gpzc.aMW0ieNrirCMYSHQtlV-TA"
        self.driver.get(url)
        self.driver.set_window_position(0, 0)
        time.sleep(2)#allow page to load
        #driver.set_window_size(10, 10)
        assert "Amazon" in self.driver.title
        email = self.driver.find_element_by_name("email")
        email.clear()
        email.send_keys("48hourmonogram@gmail.com")
        passwrd = self.driver.find_element_by_name("password")
        passwrd.clear()
        passwrd.send_keys("Drag5104")
        passwrd.send_keys(Keys.RETURN)  
        time.sleep(2)#allow page to load
        twostep = self.driver.find_element_by_id("auth-mfa-otpcode")
        twostep.clear()
        twostep.send_keys(code)
        twostep.send_keys(Keys.RETURN)  

    def pullAmOrderId(self):
        url = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=ag_myo_tnav_xx_"
        #url = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=xx_myo_qlu1_myo?byDate=shipDate&statusFilter=ItemsToShip&ignoreSearchType=1&searchFulfillers=mfn&searchType=OrderStatus&_encoding=UTF8&showCancelled=0&sortBy=OrderStatusDescending&shipSearchDateOption=noTimeLimit"
        try:
            try:
                self.driver.get(url)
            except: 
                self.driver = webdriver.Firefox()
                self.driver.get(url)
            self.driver.set_window_position(0, 0)
            time.sleep(2)#allow page to load
            try: 
                assert "Manage Orders - Amazon" in self.driver.title
            except:
                Amazon.login(self)
               #print('logged in')
                time.sleep(2)#allow page to load
                self.driver.get(url)
            self.status.set("Pulling Order Numbers - Amazon")
            orderID_List = []
            while True:
                time.sleep(3)#allow page to load
                orderID_tag = self.driver.find_elements_by_css_selector("strong")
                i = 0
                while i < len(orderID_tag):
                    data = orderID_tag[i].get_attribute('innerText')
                    ordernumbers = re.findall(r'\d{3}-\d{7}-\d{7}', data)
                    ###print (ordernumbers)
                    ordernumbers = str(ordernumbers)
                    
                    if ordernumbers != '[]':
                        ordernumbers = ordernumbers.replace("['","")
                        ordernumbers = ordernumbers.replace("']","")
                        orderID_List.append(ordernumbers) 
                    i+=1 
                try:
                    next = self.driver.find_element_by_link_text('Next')
                    next.click()
                    #raise ValueError('NoSuchElementException')
                    continue
            
                except NoSuchElementException:
                    ###print('made it this far')
                    return orderID_List
        except Exception as error:
            self.status.set(error)
            with open('Errors/Error_Report_'+Amazon.now+'.txt', 'a') as errors:
                errors.write('Amazon Error: ' + str(error) + ' - ' + Amazon.dateActual + '; ')
            return

    def getamznorderdataList(self, amazon_orders):
        i = 0
        ord = []
        leg = []
        if amazon_orders:
            while i < len(amazon_orders):
                #ordersad = '112-1034344-9758660'
                order = Amazon.pullAmOrderData(self, amazon_orders[i])
                i+=1
        else:
            self.status.set("Error - Restarting order check")
            Amazon.pullAmOrderId(self)
            

    def pullAmOrderData(self, orderNum):
        totp = pyotp.TOTP("4DBGALAT52K5RAKZ3FFJMSIB3GMIIAJDFX6TBO6EY3JN2RSI4DGA")
        code = totp.now()
        ###Returns an array of order data based on amazon order number ###-#######-#######
        l = 0
        address_list = {}
        self.driver.set_window_position(0, 0)
        url = "https://sellercentral.amazon.com/hz/orders/details?_encoding=UTF8&orderId="+orderNum+"&ref_=ag_orddet_cont_myo"
        try:
            self.driver.get(url)
            time.sleep(4)
            #assert "Amazon" in self.driver.title
            try:
                pDate_box = self.driver.find_element_by_id('myo-order-details-purchase-date')
                purchDate = pDate_box.get_attribute('innerText')
                month = purchDate[5:8].strip()
               #print(month)
                date = purchDate[8:10].strip()
                if int(date) < 10:
                    date = '0' + date
               #print(date)
                year = purchDate[12:17].strip()
               #print(year)
                with open('SKU_LIST/months.csv', 'r') as file:
                    reader = csv.reader(file)
                    for row in reader:
                        if row[0] == month:
                            month = row[1]
                purchDate = year + '-' + month + '-' + date + ' ' + time.strftime("%H:%M")
               #print(purchDate)
            except NoSuchElementException:
                purchDate = 'Error: Not able to locate data'
            try:
                buyer_box = self.driver.find_element_by_id('contact_buyer_link')
                buyer = buyer_box.get_attribute('innerText') 
                ###print('buyer')
            except NoSuchElementException:
                buyer = "Error: Not able to locate data"
            try:
                shipServ_box = self.driver.find_element_by_id('myo-order-details-order-shipping-speed')
                shipServ = shipServ_box.get_attribute('innerText')
                ###print('serv')
            except NoSuchElementException:
                shipServ = 'Error: Not able to locate data'
            try:    
                shipBy_box = self.driver.find_element_by_id('myo-order-details-order-ship-by')
                shipBy = shipBy_box.get_attribute('innerText')
                ###print('by')
            except NoSuchElementException:
                shipBy = "Error: Not able to locate data"
            try:    
                deliverBy_box = self.driver.find_element_by_id('myo-order-details-order-estimated-deliver-by')
                deliverBy = deliverBy_box.get_attribute('innerText')
               #print('dby')
            except NoSuchElementException:
                deliverBy = "Error: Not able to locate data"
            try:
                time.sleep(2)
                orderedbox = self.driver.find_element_by_id('myo-order-details-item-quantity-ordered')
                orderedbox = orderedbox.get_attribute('innerText')
               #print(orderedbox)
                shippedbox = self.driver.find_element_by_id('myo-order-details-item-quantity-shipped')
                shippedbox = shippedbox.get_attribute('innerText')
               #print(shippedbox)
                if (int(orderedbox) > int(shippedbox)):
                    status = 'Unshipped'
                   #print(status)
                else:
                    status = 'Shipped'
                   #print(status)
            except NoSuchElementException:
                status = 'Canceled - Please Confirm'
               #print(status)

            try:
               #print('add')
                address_box = self.driver.find_element_by_id('myo-order-details-buyer-address')
                customer_address = address_box.get_attribute('innerText')
                addresslist = [y for y in (x.strip() for x in customer_address.splitlines()) if y]
                address_list['shipto'] = addresslist[0]
                address_list['street'] = addresslist[1]
                address_list['locale'] = addresslist[2]
                address_list['country'] = 'United States'
                
            except NoSuchElementException:
                address_list = ["Error: Not able to locate data"]
            try:
                quantityList = []
                quantityBox =  self.driver.find_elements_by_id('myo-order-details-item-quantity-ordered')
                for items in quantityBox:
                    items=items.get_attribute('innerText')
                    quantityList.append(items)
            except NoSuchElementException:
                quantityList = ['1']
            zzzz = 0
            prod_status = []
            while zzzz < len(quantityList):
                prod_status.append("Design")
                zzzz = zzzz + 1
            try:
                orderdata = self.driver.find_elements_by_id('myo-order-details-item-product-details')
                i = 0
                od_list = []
                while i < len(orderdata):   
                    data = orderdata[i].get_attribute('innerText')
                    final = re.sub('Show more',' ', data.strip()) 
                    final.strip()
                    #print(final)
                    od_list.append([y for y in (x.strip() for x in final.splitlines()) if y])
                    i+=1
               #print('order')
            except NoSuchElementException:
                od_list = ' '
            finally:
               #print('loadingdictionary')
                newodobj = {}
                newodobj = Amazon.parseamorder(self, od_list)
                shop = 'A'
                order_List = {}
                order_List['buyer'] = buyer
                order_List['address_list'] = json.dumps(address_list)
                order_List['shipBy'] = shipBy
                order_List['deliverBy'] = deliverBy
                order_List['shipServ'] = shipServ
                order_List['od_list'] = json.dumps(newodobj)
                order_List['shop'] = shop
                order_List['purchDate'] = purchDate
                order_List['orderNum'] = orderNum
                order_List['quantityList'] = json.dumps(quantityList)
                order_List['status'] = status
                order_List['prod_status'] = json.dumps(prod_status)
                
                self.insert_order(order_List)
                return
        except Exception as error:
            self.status.set(error)
            with open('Errors/Error_Report_'+Amazon.now+'.txt', 'a') as errors:
                errors.write('Amazon Error: ' + str(error) + ' - ' + Amazon.dateActual + '; ')
            return

    def parseamorder(self, order):
       #print(order)
        od_list = {}
        items = []
        descriptions = []
        colors = []
        dots = []
        sizes = []
       #print('attemptingparse')
        xx = 0
        try:
            while xx < len(order):       
                try:
                    item = re.sub('\,', ' / ', order[xx][1].strip())
                    item = item.replace('SKU: ', '').strip()
                    item = self.skuRef(item)
                    #item = item.replace('12-36 inch ','')'
                    items.append(item)
                #print(items)
                except IndexError:
                    
                    item = ' ' 
                    items.append(item)            
                try:
                    size = re.sub('\,', ' ', order[xx][7].strip())
                    size = size.replace('Size:', '').strip()
                    size = size.replace('Size and # of Letters:', '').strip() 
                    size = size.replace('inches (Most Popular)', '').strip()
                    size = size.replace('inches', '').strip()
                    size = size.replace('letters', '').strip()
                    size = size.replace('Inches', '').strip()
                    size = size.replace('Letters', '').strip()
                    size = size.replace('(single letter only)', '').strip()
                    size = size.replace('10-12', '').strip()
                    size = size.replace('5-6', '').strip()
                    size = size.replace('7-9', '').strip()
                    size = size.replace('1-4', '').strip()
                    size = size.replace('"', '').strip()
                    sizes.append(size)
                except IndexError:
                    size = ' '
                    sizes.append(size)
                #print(sizes)
                try:
                    descript = re.sub('\,', ' ', order[xx][8].strip())
                    descript = descript.replace('Name: ','').strip()
                    descript = descript.replace('Monogram Letter: ','').strip()
                    descript = descript.replace('Monogram Letters: ','').strip()
                    descript = descript.replace('Letters: ','').strip()
                    descript = descript.replace('Letter: ','').strip()
                    descript = descript.replace('Letter to be cut:','').strip()
                    descriptions.append(descript)
                except IndexError:
                    try:
                        descript = order[xx][0].strip()
                    except IndexError:
                        descript = 'Could not locate'
                    #print(descript)
                try:
                    if order[xx][9][0] == "'":
                        try:
                            dot = re.sub('\,', ' ', order[xx][9].strip())
                        except IndexError:
                            dot = ' '
                        connect = 0
                        if dot == "'i' or 'j' dot connected or unconnected: Shipped Loose" or dot == "'i or 'j' dot connected or unconnected?: Shipped Loose":
                            for a in descript:
                                ####print (a)
                                if a =='j' or a == 'i':
                                    connect+=1
                            dot = 'Loose * '+str(connect)
                        else:
                            dot = ' '
                        try:
                            color = re.sub('\,', ' ', order[xx][10].strip())
                            color = color.replace('Color: ','').strip()
                            if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                                color = ' '
                        except IndexError:
                            color = ' '
                    else:
                        dot = ' '
                        color = ' '
                except IndexError:
                    dot = ' '
                    try:
                        color = re.sub('\,', ' ', order[xx][9].strip())
                        if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                            color = ' '
                    except IndexError:
                        color = ' '
                        #print('indexerror5')
                dots.append(dot)
            #print(dots)
                colors.append(color)
            #print(colors)
                xx += 1
            od_list['colors'] = colors
            od_list['items'] = items
            od_list['sizes'] = sizes
            od_list['description'] = descriptions
            od_list['connected'] = dot
        #print(od_list)
            return (od_list)
        except Exception as error:
            self.status.set(error)
            with open('Errors/Error_Report_'+Amazon.now+'.txt', 'a') as errors:
                errors.write('Amazon Error: ' + str(error) + ' - ' + Amazon.dateActual + '; ')
            return