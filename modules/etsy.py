from modules.imported import *

class Etsy:
    def etsyloggin(self):
        self.status.set("Logging in to Etsy")
        thisurl = ""
        self.driver.get(thisurl)
        time.sleep(3)#allow page to load
        email = self.driver.find_element_by_name("username")
        email.clear()
        email.send_keys("")
        passwrd = self.driver.find_element_by_name("password")
        passwrd.clear()
        passwrd.send_keys("")
        passwrd.send_keys(Keys.RETURN)
        time.sleep(3)
        return
    
    def pullEtOrderList(self):
        ###Returns list of Etsy order numbers
        self.status.set("Pulling Order Data - Etsy")
        dateActual = time.strftime('%c')
        orderID_List = []
        order_list = []
        now = time.strftime("%m-%d-%Y")
        thisurl = ""
        try:
            try:
                self.driver.get(thisurl)
            except: 
                self.driver = webdriver.Firefox()
                self.driver.get(thisurl)
            self.driver.set_window_position(0, 0)
            try:
                time.sleep(2)
                assert "Etsy - Sold Orders" in self.driver.title
            except:
                time.sleep(1)
                Etsy.etsyloggin(self)
                try:
                    time.sleep(1)
                    assert "Etsy - Sold Orders" in self.driver.title
                except:
                   #print('relocating')
                    try:
                        vieworders = self.driver.find_element_by_link_text("Orders & Shipping")
                        vieworders.click()
                    except NoSuchElementException: 
                        self.status.set('Could not locate orders - Etsy')
                        self.startgetNewOrders()
            self.status.set("Pulling Orders - Etsy")
            time.sleep(2)
            while True:
                #print('loading')
                orderID_tag = self.driver.find_elements_by_class_name('order-receipt')
                
                orderDetails = self.driver.find_elements_by_class_name('order')
                ###print(orderDetails)
                q = 0
                while q < len(orderDetails):
                    orderD = orderDetails[q].get_attribute('innerText')
                    ###print(orderD)
                    orderD = orderD.strip()
                    ###print(final)
                    order_list.append([y for y in (x.strip() for x in orderD.splitlines()) if y])
                    q+=1
                ###print(order_list)
                some = 0
                while some < len(order_list):
                    if order_list[some][0] == 'There is a pending cancellation on this order.':
                        del order_list[some]
                    some += 1
                i = 0
                while i < len(orderID_tag):
                    data = orderID_tag[i].get_attribute('innerText')
                    data = data.replace('#','')
                    ###print(data)
                    ordernumbers = re.findall(r'\d{10}', data.strip())
                    ###print (ordernumbers)
                    ordernumbers = str(ordernumbers)
                    
                    if ordernumbers != '[]':
                        ordernumbers = ordernumbers.replace("['","")
                        ordernumbers = ordernumbers.replace("']","")
                        orderID_List.append(ordernumbers) 
                    i+=1 
                
                try:
                    time.sleep(5)
                    nextpage = self.driver.find_element_by_link_text('Next Page')
                    try:
                        last = self.driver.find_element_by_class_name('next-disabled')
                        if last:
                            raise ValueError
                    except NoSuchElementException:
                        nextpage.click()

                except:
                   #print(order_list)
                    return order_list
                    
        except Exception as error:
            #self.statusLab.config(foreground="red")
            self.status.set(error)
            return

    def writeEtsyOrders(self, etsy_orders):
        c = 0
        z = 0
        x = 0
       #print('writtingetsty')
        now = time.strftime("%m-%d-%Y")
        while x < len(etsy_orders):
            #print(etsy_orders[x])
            thisorder = etsy_orders[x]
            order_List = {}
            address_list = {}
            od_list = {}
            itemList = []
            sizes = []
            colors = []
            quantity = []
            quantityList = []
            quantloc = []
            shop = 'E'
            desc = ''
            exp = ''
            p = re.compile('D/', re.IGNORECASE)
            try:
                purch = ''
                purch = re.sub('\,', ' ', thisorder[3].strip())
                month = purch[:3]
                date = purch[-8:-6].strip()
                year = purch[-4:].strip()
                with open('SKU_LIST/months.csv', 'r') as file:
                    reader = csv.reader(file)
                    for row in reader:
                        if row[0] == month:
                            month = row[1]
                if int(date) < 10:
                    date = '0' + date
                purchaseDate = year + '-' + month + '-' + date + ' ' + time.strftime("%H:%M")


            except IndexError:
                purchaseDate = now.strftime("%Y-%m-%d %H:%M")

                
            for items in thisorder:
                if items.startswith('Wooden') or items.startswith('Sale') or items.startswith('Get') or items.startswith('Fast') \
                or items.startswith('FAST!') or items.startswith('SALE') or items.startswith('Sale-') or items.startswith('24"') or \
                items.startswith('SOLD') or items.startswith('Custom') or items.startswith('12') or items.startswith('EXTRA'):
                    items = items.replace('Listing Stats','').strip()
                    items = items[-3:]
                    items = self.skuRef(items)
                    itemList.append(items)

            od_list['items'] = itemList
                
            for items in thisorder:
                if items.startswith('Height') or items.startswith('Size'):
                    items = items.replace('Height, # of Letters:' , '').strip()
                    items = items.replace('1-4', '').strip()
                    items = items.replace('5-6', '').strip()
                    items = items.replace('7-9', '').strip()
                    items = items.replace('10-12', '').strip()
                    items = items.replace('Unpainted', '').strip()
                    items = items.replace('"', '').strip()
                    items = items.replace('Size:', '').strip()
                    items = items.replace('inches', '').strip()
                    items = items.replace('Inches', '').strip()
                    items = items.replace('Letters', '').strip()
                    items = items.replace('(most popular)', '').strip()
                    sizes.append(items)
            od_list['sizes'] = sizes

            for items in thisorder:
                if items.startswith('Note'):
                    items = items.replace('Note from Buyer', '').strip()
                    items = items.replace('Send Convo','')
                    items = items.replace(',','/').strip()
                    desc = items
            od_list['description'] = desc
            for items in thisorder:
                if items.startswith("Shipping Method:"):
                    items = items.replace('Shipping Method: ', '').strip()
                    shipping = items
                    exp = ' '
            order_List['shipServ'] = shipping
            for items in thisorder:
                if items.startswith('Color'):
                    items = items.replace('Unpainted','')
                    items = items.replace('Unpainted (goes out in 48 hrs)', '')
                    colors.append(items.replace('Color: ', '').strip())
            od_list['colors'] = colors

            for items in thisorder:
                if items.startswith('#'):
                    items = items.replace('#','')
                    orderNum = items
            order_List['orderNum'] = orderNum
            indices = [o for o, s in enumerate(thisorder) if 'Quantity' in s]
            ####print(indices)
            xyz=0
            while xyz < len(indices):
                quantloc.append(int(indices[xyz]) + 1)
                xyz+=1
            for i in quantloc:
                quant = re.sub('\,', ' ', thisorder[i].strip())
                quantityList.append(int(quant))
            order_List['quantityList'] = json.dumps(quantityList)
            zzzz = 0
            prod_status = []
            while zzzz < len(quantityList):
                prod_status.append("Design")
                zzzz = zzzz + 1
            order_List['prod_status'] = json.dumps(prod_status)
            try:
                indices = [o for o, s in enumerate(thisorder) if 'Ship To' in s]
                #print(indices)
                ####print(indices)
                nameloc = int(indices[0]) + 1
                name = re.sub('\,', ' ', thisorder[nameloc].strip())
                shipname =  string.capwords(name)              
                address_list['street']= thisorder[nameloc+1]
                address_list['locale']= thisorder[nameloc+2]
                address_list['country']= thisorder[nameloc+3]
                status = 'Unshipped'
            except IndexError:
                try:
                    
                    indices = [o for o, s in enumerate(thisorder) if 'Shipped To' in s]
                    #print(indices)
                    ####print(indices)
                    nameloc = int(indices[0]) + 1
                    name = re.sub('\,', ' ', thisorder[nameloc].strip())
                    shipname =  string.capwords(name)              
                    address_list['street']= thisorder[nameloc+1]
                    address_list['locale']= thisorder[nameloc+2]
                    address_list['country']= thisorder[nameloc+3]
                    status = "Unshipped"
                    
                except IndexError:
                    shipname = re.sub('\,', ' ', thisorder[0].strip())
            
            order_List['buyer'] = shipname
            order_List['address_list'] = json.dumps(address_list)
            order_List['od_list'] = json.dumps(od_list)
            order_List['shipBy'] = 'NULL'
            order_List['deliverBy'] = 'NULL'
            order_List['status'] = status
            order_List['shop'] = 'E'
            order_List['purchDate'] = purchaseDate
           #print(order_List)
            self.insert_order(order_List)
            x += 1