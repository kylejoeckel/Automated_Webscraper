from modules.imported import *
from modules.etsy import Etsy
from modules.amazon import Amazon
from modules.ux import UX

mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = ''
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'
mysql.init_app(app)

class ScrapeApp:
    def __init__(self):
        os.environ['TZ'] = 'Pacific/Pitcairn'
        time.tzset
        time.tzset()
        UX.createUX(self)
        mainloop()

    def openBrowser(self):
        self.driver = webdriver.Firefox()
    
    def startSchedule(self):
        sched = threading.Thread(target=self.schedluler)
        sched.daemon = True
        if sched.isAlive():
            sched.join()
        sched.start()

    def startcheckThread(self):
        checkThread = threading.Thread(target=self.checkForNew)
        checkThread.daemon = True
        if checkThread.isAlive():
            checkThread.join()
        checkThread.start()

    def startcsvThread(self):
        csvThread = threading.Thread(target=self.createCSV)
        csvThread.daemon = True
        if csvThread.isAlive():
            csvThread.join()
        csvThread.start()

    def startgetNewOrders(self):
        csvThread = threading.Thread(target=self.getNewOrders)
        csvThread.daemon = True
        if csvThread.isAlive():
            csvThread.join()
        csvThread.start()

    def schedluler(self):
        self.startgetNewOrders()
        schedule.every(240).minutes.do(self.startgetNewOrders)
        schedule.every().day.at("00:00").do(self.clearCounters)
        while True:
            schedule.run_pending()
            time.sleep(1)

    def clearCounters(self):
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        self.tt.set(dateActual)
        # self.v.set(0)
        self.newEtsyOrd.set(0)
        self.newAmOrd.set(0)
        self.status.set('ready')
        return

    def warning(self):
        now = time.strftime("%m-%d-%Y")
        wait = messagebox.askokcancel('Warning', "This will clear all counters which is already set for 12:00am tonight, would you like to continue?")
        if wait == False:
            return
        else:
            self.clearCounters()

    def quitAll(self):
        wait = messagebox.askokcancel('Quit', "Are you sure you want to quit?")
        if wait == False:
            return
        try:
            self.driver.quit()
        except:
            self.body.quit()
        finally:
            self.body.quit()
            sys.exit()
 
    def insert_order(self, orderlist):
            query = ("INSERT INTO orders" 
                    "(customer_name, address_list, shipby, deliverby, shipping, order_obj, shop, purchdate, order_num, order_quantity, order_status, prod_status)" 
                    "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")
            args = (orderlist['buyer'], orderlist['address_list'], orderlist['shipBy'], orderlist['deliverBy'], orderlist['shipServ'], orderlist['od_list'], orderlist['shop'], orderlist['purchDate'], orderlist['orderNum'], orderlist['quantityList'], orderlist['status'], orderlist['prod_status'])
            try:
                self.status.set('Connecting to DB')
                conn = mysql.connect()
                try:
                    self.status.set('Attempting Data Insert')
                    time.sleep(.5)
                    cursor = conn.cursor()
                    cursor.execute(query, args)
                    conn.commit()
                    self.v.set(self.v.get()+1)
                    if orderlist['shop'] == 'A':
                        self.newAmOrd.set(self.newAmOrd.get()+1)
                    else:
                        self.newEtsyOrd.set(self.newEtsyOrd.get()+1)
                except Exception as error:
                    self.status.set(error)
                    time.sleep(.5)
                    self.status.set('Updating Order Status')
                    time.sleep(.5)
                    query = ("UPDATE orders SET order_status = %s WHERE order_num = %s")
                    args = (orderlist['status'], orderlist['orderNum'])
                    cursor = conn.cursor()
                    cursor.execute(query, args)
                    conn.commit()
            except Error as error:
                self.status.set(error)
                time.sleep(.5)
            finally:
                cursor.close()
                conn.close()

    def getNewOrders(self):
        self.status.set("Checking For New Etsy Orders/Pull Order Data")
        etsy_orders = Etsy.pullEtOrderList(self)
        Etsy.writeEtsyOrders(self, etsy_orders)
        self.status.set("Checking For New Amazon Orders")
        dateActual = time.strftime('%c')
        self.checktime.set(dateActual)
        amazon_orders = Amazon.pullAmOrderId(self)
        self.status.set("Pulling Amazon Order Data")
        am_ord = Amazon.getamznorderdataList(self, amazon_orders)
        self.status.set("Finished")
        return

    def skuRef(self, sku):
        amznskuDict = {}
        etsyskuDict = {}
        ignoreskulist = {}
        with open('SKU_LIST/ignore_sku_list.csv', 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                if row[0] == sku:
                    return 'Amazon Fullfilled'       
        with open('SKU_LIST/skuList.csv') as file:
            reader = csv.reader(file)
            for row in reader:
                etsyskuDict[row[1]] = row[0]
                amznskuDict[row[2]] = row[0]
        if sku in etsyskuDict:       
            return etsyskuDict.get(sku)
        if sku in amznskuDict:
            return amznskuDict.get(sku)
        else:
            return 'Custom Order'

if __name__ == '__main__':

    # info = AppKit.NSBundle.mainBundle().infoDictionary()
    # info['LSUIElement'] = True
    app = ScrapeApp()
    



