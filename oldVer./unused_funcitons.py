def writeAmznOrders(self, amord, filewriter):
    c = 0
    z = 0
    x = 0
    while c < len(amord): 
        try:
            date = re.sub('\,', ' ', amord[c][7].strip())
            #purchaseDate = date.replace(date[:4], '').strip()
        except IndexError:
            date = ''
        try:    
            shop = re.sub('\,', ' ', amord[c][6].strip())
        except IndexError:
            shop = ' '
        xx = 0
        while xx < len(amord[c][5]):       
            try:
                item = re.sub('\,', ' / ', amord[c][5][xx][1].strip())
                item = item.replace('SKU: ', '').strip()
                item = self.skuRef(item)
                items.append(item)
                #item = item.replace('12-36 inch ','')
            except IndexError:
                item = ' '             
            try:
                size = re.sub('\,', ' ', amord[c][5][xx][7].strip())
                size = size.replace('Size:', '').strip()
                size = size.replace('Size and # of Letters:', '').strip() 
                size = size.replace('inches (Most Popular)', '').strip()
                size = size.replace('inches', '').strip()
                size = size.replace('letters', '').strip()
                size = size.replace('Inches', '').strip()
                size = size.replace('Letters', '').strip()
                size = size.replace('(single letter only)', '').strip()
                size = size.replace('10-12', '').strip()
                size = size.replace('5-6', '').strip()
                size = size.replace('7-9', '').strip()
                size = size.replace('1-4', '').strip()
                size = size.replace('"', '').strip()
            except IndexError:
                size = ' '
            try:
                descript = re.sub('\,', ' ', amord[c][5][xx][8].strip())
                descript = descript.replace('Name: ','').strip()
                descript = descript.replace('Monogram Letter: ','').strip()
                descript = descript.replace('Monogram Letters: ','').strip()
                descript = descript.replace('Letters: ','').strip()
                descript = descript.replace('Letter: ','').strip()
                descript = descript.replace('Letter to be cut:','').strip()
            except IndexError:
                descript = ' '
            try:   
                name = re.sub('\,', ' ', amord[c][1][0].strip())
            except IndexError:
                name = " "
            if amord[c][4] == "Shipping Service: Standard":
                exp = ' '
            else:
                exp = 'E'
            try:
                quantity = amord[c][9]
                #####print (quantity)
            except IndexError:
                quantity = '1'
            try:
                if amord[c][5][xx][9][0] == "'":
                    try:
                        dot = re.sub('\,', ' ', amord[c][5][xx][9].strip())
                    except IndexError:
                        dot = ' '
                    connect = 0
                    if dot == "'i' or 'j' dot connected or unconnected: Shipped Loose" or dot == "'i or 'j' dot connected or unconnected?: Shipped Loose":
                        for a in descript:
                            ####print (a)
                            if a =='j' or a == 'i':
                                connect+=1
                        dot = 'Loose * '+str(connect)
                    else:
                        dot = ' '
                    try:
                        color = re.sub('\,', ' ', amord[c][5][xx][10].strip())
                        color = color.replace('Color: ','').strip()
                        if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                            color = ' '
                    except IndexError:
                        color = ' '
                else:
                    dot = ' '
                    color = ' '
            except IndexError:
                dot = ' '
                try:
                    color = re.sub('\,', ' ', amord[c][5][xx][9].strip())
                    if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                        color = ' '
                except IndexError:
                    color = ' '
            xxx = 0
            try:
                while xxx < int(quantity[xx]):
                    filewriter.writerow([date, shop, size, item, color,' ', descript, dot, name, ' ', exp])
                    self.newAmOrd.set(self.newAmOrd.get()+1)
                    self.v.set(self.v.get()+1)
                    xxx += 1
            except IndexError:
                ###print('write failed')
                break
            xx += 1
        return od_list



def checkNewLoop(self,arg):
    if arg == 'True':
        t=threading.Timer(900.0,lambda: self.checkNewLoop('True'))
        t.daemon = True
        t.start()
        self.checkForNew()


def checkForNew(self):
    now = time.strftime("%m-%d-%Y")
    if not os.path.exists('Daily_Spread/orders_'+now+'.csv'):
        self.createCSV()
    self.status.set("Checking...")
    oldEtsy = []
    oldAm = []
    newAmOrders = 0
    newEtsyOrders = 0
    #newAm = self.pullAmOrderList()
    self.status.set("Checking for New Etsy Orders")
    with open("etsy_orders/etsy_order_numbers.txt", "r") as ordernumFile:
        for line in ordernumFile:
            oldEtsy.append(line.strip())

        ####print(oldEtsy)
    newEtsy = self.pullEtsyOrderId()
    ####print (newEtsy)
    for val in newEtsy:
        if val not in oldEtsy:
            newEtsyOrders+=1
    # ####print(newEtsyOrders)
    self.newEtsyOrd.set(self.newEtsyOrd.get()+newEtsyOrders)
    self.status.set("Checking for New Amazon Orders")
    with open("amzn_orders/amzn_order_numbers.txt", "r") as ordernumFile:
        for line in ordernumFile:
            oldAm.append(line.strip())
    newAm = self.pullAmOrderId()
    for val in newAm:
        if val not in oldAm:
            newAmOrders+=1
    # ####print(newAmOrders)
    #self.newAmOrd.set(self.newAmOrd.get()+newAmOrders)
    newOrders = newAmOrders + newEtsyOrders
    dTime = time.strftime("%I:%M:%S")
    dateActual = time.strftime('%c')
    self.checktime.set(dateActual)
    #oldcount = self.v.get()
    #ordercount = oldcount + newOrders
    #self.v.set(ordercount)
    self.status.set('ready')
    if newOrders > 0:
        self.startgetNewOrders()
    return  








        
    def build(self):
    #create a button, and  attach animate() method as a on_press handler
    #layout = BoxLayout(padding=0)
    layout1 = BoxLayout(padding=10, orientation='vertical')
    layout = BoxLayout(padding = 10)
    button = Button(text="Search for new orders...")
    button.bind(on_press=self.checkForNew)
    button2 = Button(text='Create Spead Sheet')
    button2.bind(on_press=self.createCSV)
    layout.add_widget(button)
    layout.add_widget(button2)
    l = Label(text="48 Hour Monogram")
    Clock.schedule_interval(lambda x: self.update_label(x, new), 700)
    #x.bind(on_press=self.checkForNew)
    #x.text = new
    layout1.add_widget(l)
    new = self.checkForNew()
    x = Label(text=new)
    layout1.add_widget(x)

    layout1.add_widget(layout)

    #layout.add_widget(btn)
    return layout1