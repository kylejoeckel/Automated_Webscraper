from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from datetime import datetime, timedelta
import os, time
import pytz
import _thread
import threading
import string
import time
import schedule
import re
import csv
import subprocess
import AppKit
import sys

'''import kivy
kivy.require('1.0.7')

from kivy.animation import Animation
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.clock import Clock'''

from tkinter import *
from tkinter import ttk



class FortyEightApp:
    def __init__(self):
        self.body = Tk()
        #self.top.bind('<Return>', self.click)
        self.body.configure(bg='white')
        self.body.title('FortyEight')
        self.body.maxsize(width=900, height=800)
        
        #self.top.
        #MENUBAR#######################################################
        self.menubar = Menu(self.body)#.pack(side=RIGHT, fill=BOTH)
        #self.menuBar.add_cascade(label="File", menu=self.menuBar)
        self.menubar.add_command(label="Quit", command=self.body.quit)
        self.body.configure(menu=self.menubar)
        self.logo = PhotoImage(file='art/Black.gif')
        #WELCOMEFRAME##################################################
        self.welcomeFrame = Frame(self.body)
        self.welcomeTag = Label(self.welcomeFrame, text = '48 Hour Monogram').pack(side=LEFT,fill=BOTH)
        #self.welcomeFrame.pack(side=TOP, fill=BOTH)
        #LOGOANDTOTAL########################################s
        #self.topFrame = Frame(self.body)
        self.logoFrame = Frame(self.body)
        self.logoLab = Label(self.logoFrame,image=self.logo)
        self.logoLab.image = self.logo
        self.logoLab.pack(padx=30, side=LEFT)
        self.logoFrame.grid(row=0,column=0)#pack(fill=BOTH, side=LEFT)
        self.totalFrame = Frame(self.body, bg = 'black', bd=1, relief=SUNKEN)
        self.v = IntVar()
        self.v.set("0")
        self.status = StringVar()
        self.status.set('Idle')
        self.totalLal = Label(self.totalFrame, textvariable = self.v, font=('San Francisco', 78), bg = 'black', fg='green', wraplength=400).pack(padx=10,fill=BOTH)
        staticstat = Label(self.totalFrame, text="Current Status: ", font=('San Francisco', 28), bg = 'black', fg='blue', wraplength=400 ).pack(padx=10,fill=BOTH)
        self.statusLab = Label(self.totalFrame, textvariable = self.status, font=('San Francisco', 28), bg = 'black', fg='red', wraplength=400).pack(padx=10,fill=BOTH)
        self.totalFrame.grid(row=0,column=1)#pack(padx=30, pady=30, side=RIGHT, anchor=NE)
        #self.topFrame.grid(row=0, column=0, rowspan=4, columnspan=2)#pack(fill=BOTH)
        #total#######
        #
        #self.totalFrame.pack(side=RIGHT, anchor=NE)
        #INFOFRAME#####################################################
        self.infoFrame = Frame(self.body,bd=1, relief=SUNKEN)
        self.checktime = StringVar()
        self.newEtsyOrd = IntVar()
        self.newAmOrd = IntVar()
        self.tt = StringVar()
        self.newEtsyOrd.set(0)
        self.newAmOrd.set(0)
        self.tt.set('-:--')
        self.checktime.set('-:--')
        self.etsylab = Label(self.infoFrame, text = 'New Etsy Orders: ', font=('San Francisco', 20)).grid(sticky='w',row=0,column=0)
        self.newetsyorders = Label(self.infoFrame, textvariable=self.newEtsyOrd, font=('San Francisco', 20)).grid(sticky='w', row=0, column=1)#pack(padx=10, pady=5, side=TOP,fill=BOTH, anchor=W)
        self.amlab = Label(self.infoFrame, text = 'New Amazon Orders: ', font=('San Francisco', 20)).grid(sticky='w',row=1,column=0)      
        self.newamorders = Label(self.infoFrame, textvariable=self.newAmOrd, font=('San Francisco', 20)).grid(sticky='w', row=1,column=1)#pack(padx=10, pady=5, side=TOP,fill=BOTH, anchor=W)
        self.lastchecklab = Label(self.infoFrame, text = 'Last Order Check: ', font=('San Francisco', 20)).grid(sticky='w',row=2,column=0)
        self.lastCheck = Label(self.infoFrame, textvariable=self.checktime, font=('San Francisco', 20)).grid(sticky='w', row=2, column=1)#pack(padx=10, pady=5, side=TOP,fill=BOTH, anchor=W)
        self.lastcsvlab = Label(self.infoFrame, text = 'Last CSV Created: ', font=('San Francisco', 20)).grid(sticky='w',row=3,column=0)
        self.lastCSV = Label(self.infoFrame, textvariable=self.tt, font=('San Francisco', 20)).grid(sticky='w', row=3, column=1)#pack(padx=10, pady=5, side=BOTTOM,fill=BOTH, anchor=W)
        self.infoFrame.grid(padx=10,pady=10,row=5, column=1)#pack(side=RIGHT, anchor=E)
        #ORDERCHECKFRAME###############################################
        self.orderCheckFrame = Frame(self.body, bd=1, relief=SUNKEN)
        self.csv = Button(self.orderCheckFrame, command=self.startcsvThread, text='Create CSV', width=25)
        self.check = Button(self.orderCheckFrame, command=self.startcheckThread, text='Check for New Orders',width=25)
        self.l = Label(self.orderCheckFrame, text="Select an Option Below:", font=('San Francisco', 20))
        self.l.grid(sticky='w',row=0)#pack(padx=10, pady=10, fill=BOTH)
        self.check.grid(sticky='w',row=1)#pack(padx=10, pady=5, fill=BOTH)
        self.csv.grid(sticky='w',row=2)#pack(fill=BOTH,padx=10, pady=5)
        self.quitbutton = Button(self.orderCheckFrame,command=self.quitAll, text='Quit X', width=25).grid(sticky='w',row=3)#pack(fill=BOTH,padx=10, pady=5)
        self.orderCheckFrame.grid(padx=10,pady=10,row=5, column=0)#pack(side=LEFT, anchor=SW)
        #self.checkForNew()
        sched = threading.Thread(target=self.schedluler)
        sched.daemon = True
        sched.start()
        #self.checkForNew()
        mainloop()
    def startcheckThread(self):
        checkThread = threading.Thread(target=self.checkForNew)
        checkThread.daemon = True
        if checkThread.isAlive():
            checkThread.join()
        checkThread.start()
    def startcsvThread(self):
        csvThread = threading.Thread(target=self.createCSV)
        csvThread.daemon = True
        if csvThread.isAlive():
            csvThread.join()
        csvThread.start()
    def startwriteCSV(self):
        csvThread = threading.Thread(target=self.writeCSV)
        csvThread.daemon = True
        if csvThread.isAlive():
            csvThread.join()
        csvThread.start()

    def schedluler(self):
        os.environ['TZ'] = 'Pacific/Pitcairn'
        time.tzset
        time.tzset()
        schedule.every().day.at('00:00').do(self.createCSV)
        schedule.every(10).minutes.do(self.checkForNew)
        while True:
            schedule.run_pending()
            time.sleep(1)    
    
    def pullEtsyOrderId(self):
        i = 0
        orderID_List = []
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        driver = webdriver.Firefox()#Need to brew install geckodriver
        url = "https://www.etsy.com/your/orders/sold?ref=seller-platform-mcnav"  
        try:
            driver.get(url)
            driver.set_window_position(0, 0)
            assert "Etsy" in driver.title
            time.sleep(4)
            email = driver.find_element_by_name("username")
            email.clear()
            email.send_keys("48HourMonogram")
            passwrd = driver.find_element_by_name("password")
            passwrd.clear()
            passwrd.send_keys("economos13")
            passwrd.send_keys(Keys.RETURN)  
            while True:
                i=0
                time.sleep(5)#allow page to load
                orderID_tag = driver.find_elements_by_class_name('order-receipt')
                while i < len(orderID_tag):
                    data = orderID_tag[i].get_attribute('innerText')
                    data = data.replace('#','')
                    #print(data)
                    ordernumbers = re.findall(r'\d{10}', data.strip())
                    #print (ordernumbers)
                    ordernumbers = str(ordernumbers)
                    
                    if ordernumbers != '[]':
                        ordernumbers = ordernumbers.replace("['","")
                        ordernumbers = ordernumbers.replace("']","")
                        orderID_List.append(ordernumbers) 
                    i+=1 
                try:
                    next = driver.find_element_by_link_text('Next Page')
                    try:
                        next = driver.find_element_by_class_name('next-disabled')
                        raise ValueError
                    except NoSuchElementException:
                        next.click()
                except NoSuchElementException:
                    time.sleep(10)
                    next = driver.find_element_by_link_text('Next Page')
                    try:
                        next == driver.find_element_by_class_name('next-disabled')
                        raise ValueError
                    except NoSuchElementException:
                        next.click()
            #continue
                except ValueError:
                    driver.quit()
                    return orderID_List
        except:
            error = "Error: Logging in to Etsy"
            #self.statusLab.config(fg="red")
            self.status.set(error)
            with open('Errors/Error_Report_'+dateActual+'.txt', 'a') as errors:
                errors.write(error + ' - ' + dateActual)
            driver.quit()
            return

        
    def pullEtOrderList(self):
        ###Returns list of Etsy order numbers
        dateActual = time.strftime('%c')
        orderID_List = []
        order_list = []
        now = time.strftime("%m-%d-%Y")
        driver = webdriver.Firefox()#Need to brew install geckodriver
        url = "https://www.etsy.com/your/orders/sold?ref=seller-platform-mcnav"
        try:
            driver.get(url)
            driver.set_window_position(0, 0)
            assert "Etsy" in driver.title
            time.sleep(4)
            email = driver.find_element_by_name("username")
            email.clear()
            email.send_keys("48HourMonogram")
            passwrd = driver.find_element_by_name("password")
            passwrd.clear()
            passwrd.send_keys("economos13")
            passwrd.send_keys(Keys.RETURN)  
            time.sleep(5)
            while True:
                time.sleep(5)#allow page to load
                orderID_tag = driver.find_elements_by_class_name('order-receipt')
                orderDetails = driver.find_elements_by_class_name('order')
                #print(orderDetails)
                q = 0
                
                while q < len(orderDetails):
                    orderD = orderDetails[q].get_attribute('innerText')
                    #print(orderD)
                    orderD = orderD.strip()
                    #print(final)
                    order_list.append([y for y in (x.strip() for x in orderD.splitlines()) if y])
                    q+=1
                #print(order_list)
                some = 0
                while some < len(order_list):
                    if order_list[some][0] == 'There is a pending cancellation on this order.':
                        del order_list[some]
                    some += 1
                i = 0
                while i < len(orderID_tag):
                    data = orderID_tag[i].get_attribute('innerText')
                    data = data.replace('#','')
                    #print(data)
                    ordernumbers = re.findall(r'\d{10}', data.strip())
                    #print (ordernumbers)
                    ordernumbers = str(ordernumbers)
                    
                    if ordernumbers != '[]':
                        ordernumbers = ordernumbers.replace("['","")
                        ordernumbers = ordernumbers.replace("']","")
                        orderID_List.append(ordernumbers) 
                    i+=1 
                try:
                    next = driver.find_element_by_link_text('Next Page')
                    try:
                        next == driver.find_element_by_class_name('next-disabled')
                        raise ValueError
                    except NoSuchElementException:
                        next.click()
                except NoSuchElementException:
                    time.sleep(10)
                    next = driver.find_element_by_link_text('Next Page')
                    try:
                        next == driver.find_element_by_class_name('next-disabled')
                        raise ValueError
                    except NoSuchElementException:
                        next.click()

                except ValueError:
                    driver.quit()
                    with open("etsy_orders/etsy_order_numbers.txt", "r") as ordernumFile:
                        for line in ordernumFile:
                            line = line.strip()
                            line = line.replace("['","")
                            line = line.replace("']","")
                            #print(line)
                            p = 0
                            #print (orderID_List[p])
                            while p < len(orderID_List): 
                                if line == orderID_List[p]:
                                    #print(orderID_List[p])
                                    someother = 0
                                    while someother < len(order_list):
                                        for twip in order_list[someother]:
                                            twip = twip.replace('#','')
                                            if twip == line:
                                                #print(order_list[someother])
                                                del order_list[someother]
                                        someother+=1
                                    del orderID_List[p]
                                p+=1
                    
                    with open("etsy_orders/data/etsy_order_data_"+now+".json", "a") as output:
                            output.write(str(order_list))
                    return order_list
        except:
            error = "Error: Logging in to Etsy"
            #self.statusLab.config(foreground="red")
            self.status.set(error)
            with open('Errors/Error_Report_'+dateActual+'.txt', 'a') as errors:
                errors.write(error + ' - ' + dateActual)
            driver.quit()
            return

    def pullAmOrderId(self):
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        driver = webdriver.Firefox()#Need to brew install geckodriver
        url = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=ag_myo_tnav_xx_"
        try:
            driver.get(url)
            driver.set_window_position(0, 0)
            #driver.set_window_size(10, 10)
            assert "Amazon" in driver.title
            email = driver.find_element_by_name("email")
            email.clear()
            email.send_keys("48hourmonogram@gmail.com")
            passwrd = driver.find_element_by_name("password")
            passwrd.clear()
            passwrd.send_keys("Drag5104")
            passwrd.send_keys(Keys.RETURN)      
            orderID_List = []
            while True:
                time.sleep(3)#allow page to load
                orderID_tag = driver.find_elements_by_css_selector("strong")
                i = 0
                while i < len(orderID_tag):
                    data = orderID_tag[i].get_attribute('innerText')
                    ordernumbers = re.findall(r'\d{3}-\d{7}-\d{7}', data)
                    #print (ordernumbers)
                    ordernumbers = str(ordernumbers)
                    
                    if ordernumbers != '[]':
                        ordernumbers = ordernumbers.replace("['","")
                        ordernumbers = ordernumbers.replace("']","")
                        orderID_List.append(ordernumbers) 
                    i+=1 
                try:
                    next = driver.find_element_by_link_text('Next')
                    next.click()
                    #raise ValueError('NoSuchElementException')
                    continue
                except NoSuchElementException:
                    driver.quit()
                return orderID_List
        except:
            error = "Error: Logging in to Amazon"
            #self.statusLab.config(foreground="red")
            self.status.set(error)
            with open('Errors/Error_Report_'+dateActual+'.txt', 'a') as errors:
                errors.write(error + ' - ' + dateActual)
            driver.quit()
            return

    def pullAmOrderList(self):
        ###Returns list of Amazon order numbers
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        orderID_List = self.pullAmOrderId()
        with open("amzn_orders/amzn_order_numbers.txt", "r") as ordernumFile:
            for line in ordernumFile:
                line = line.strip()
                line = line.replace("['","")
                line = line.replace("']","")
                #print(line)
                p = 0
                #print (orderID_List[p])
                while p < len(orderID_List): 
                    if line == orderID_List[p]:
                        #print(orderID_List[p])
                        del orderID_List[p]
                        p+=1
                        continue
                    else:
                        #print(orderID_List[p])
                        p+=1
                        continue 
        return orderID_List

    def pullAmOrderData(self, orderNum):
        ###Returns an array of order data based on amazon order number ###-#######-#######
        l = 0
        dateActual = time.strftime('%c')
        now = time.strftime("%m-%d-%Y")
        driver = webdriver.Firefox()#Need to brew install geckodriver 
        driver.set_window_position(0, 0)
        #driver.set_window_size(10, 10)
        url = "https://sellercentral.amazon.com/hz/orders/details?_encoding=UTF8&orderId="+orderNum+"&ref_=ag_orddet_cont_myo"
        try:
            driver.get(url)
            time.sleep(3)
            assert "Amazon" in driver.title
            email = driver.find_element_by_name("email")
            email.clear()
            email.send_keys("48hourmonogram@gmail.com")
            passwrd = driver.find_element_by_name("password")
            passwrd.clear()
            passwrd.send_keys("Drag5104")
            passwrd.send_keys(Keys.RETURN)
            time.sleep(4)#allow page to load
            try:
                pDate_box = driver.find_element_by_id('myo-order-details-purchase-date')
                purchDate = pDate_box.get_attribute('innerText')
            except NoSuchElementException:
                purchDate = 'Error: Not able to locate data'
            try:
                buyer_box = driver.find_element_by_id('contact_buyer_link')
                buyer = 'Buyer: '+buyer_box.get_attribute('innerText') 
            except NoSuchElementException:
                buyer = "Error: Not able to locate data"
            try:
                shipServ_box = driver.find_element_by_id('myo-order-details-order-shipping-speed')
                shipServ = "Shipping Service: "+shipServ_box.get_attribute('innerText')
            except NoSuchElementException:
                shipServ = 'Error: Not able to locate data'
            try:    
                shipBy_box = driver.find_element_by_id('myo-order-details-order-ship-by')
                shipBy = "Ship By: "+shipBy_box.get_attribute('innerText')
            except NoSuchElementException:
                shipBy = "Error: Not able to locate data"
            try:    
                deliverBy_box = driver.find_element_by_id('myo-order-details-order-estimated-deliver-by')
                deliverBy = "Deliver By: "+deliverBy_box.get_attribute('innerText')
            except NoSuchElementException:
                deliverBy = "Error: Not able to locate data"
            try:
                address_box = driver.find_element_by_id('myo-order-details-buyer-address')
                customer_address = address_box.get_attribute('innerText')
                address_list = [y for y in (x.strip() for x in customer_address.splitlines()) if y]
            except NoSuchElementException:
                address_list = ["Error: Not able to locate data"]
            try:
                quantityList = []
                quantityBox =  driver.find_elements_by_id('myo-order-details-item-quantity-ordered')
                for items in quantityBox:
                    items=items.get_attribute('innerText')
                    quantityList.append(items)
                #print(quantityList)
                #quantityList = [y for y in (x.strip() for x in quantity.splitlines()) if y]
            except NoSuchElementException:
                quantityList = ['1']
            try:
                orderdata = driver.find_elements_by_id('myo-order-details-item-product-details')
                i = 0
                od_list = []
                while i < len(orderdata):   
                    data = orderdata[i].get_attribute('innerText')
                    final = re.sub('Show more',' ', data.strip()) 
                    final.strip()
                    #print(final)
                    od_list.append([y for y in (x.strip() for x in final.splitlines()) if y])
                    i+=1
                
            except NoSuchElementException:
                od_list = ' '
            while l < len(od_list):
                if od_list[l][0].startswith('Legging'):
                    shop = 'A'
                    leg_List = []
                    leg_List.append(buyer)
                    leg_List.append(address_list)
                    leg_List.append(shipBy)
                    leg_List.append(deliverBy)
                    leg_List.append(shipServ)
                    leg_List.append(od_list)
                    leg_List.append(shop)
                    leg_List.append(purchDate)
                    leg_List.append(orderNum)
                    assert "No results found." not in driver.page_source
                    driver.quit()
                    with open("amzn_orders/amzn_legging_orders"+now+".json", "a") as output2:
                        output2.write(str(leg_List))
                    return "Legging Order"

                else:
                    shop = 'A'
                    order_List = []
                    order_List.append(buyer)
                    order_List.append(address_list)
                    order_List.append(shipBy)
                    order_List.append(deliverBy)
                    order_List.append(shipServ)
                    order_List.append(od_list)
                    order_List.append(shop)
                    order_List.append(purchDate)
                    order_List.append(orderNum)
                    order_List.append(quantityList)
                    assert "No results found." not in driver.page_source
                    driver.quit()
                    with open("amzn_orders/data/amzn_order_data_"+now+".json", "a") as output:
                        output.write(str(order_List))
                    return order_List
        except:
            error = "Error: Logging in to Amazon"
            #self.statusLab.config(foreground="red")
            self.status.set(error)
            with open('Errors/Error_Report_'+dateActual+'.txt', 'a') as errors:
                errors.write(error + ' - ' + dateActual)
            driver.quit()
            return


    def getamznorderdataList(self, amazon_orders):
        i = 0
        ord = []
        leg = []
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        while i < len(amazon_orders):
            #ordersad = '112-1034344-9758660'
            order = self.pullAmOrderData(amazon_orders[i])
            if order != "Legging Order":
                ord.append(order)
            else:
                leg.append(order)
            i+=1
        return ord

    def writeEtsyOrders(self, etsy_orders, filewriter):
        c = 0
        z = 0
        x = 0
        now = time.strftime("%m-%d-%Y")
        with open("etsy_orders/data/etsy_order_data_"+now+".json", "a") as output:
            output.write(str(etsy_orders))
        while x < len(etsy_orders):
            itemList = []
            sizes = []
            colors = []
            quantity = []
            quantityList = []
            quantloc = []
            shop = 'E'
            desc = ''
            exp = ''
            try:
                purchaseDate = re.sub('\,', ' ', etsy_orders[x][3].strip())
            except IndexError:
                purchaseDate = ' '
                
            for items in etsy_orders[x]:
                if items.startswith('Wooden') or items.startswith('Sale') or items.startswith('Get') or items.startswith('Fast') \
                or items.startswith('FAST!') or items.startswith('SALE') or items.startswith('Sale-') or items.startswith('24"') or \
                items.startswith('SOLD') or items.startswith('Custom') or items.startswith('12') or items.startswith('EXTRA'):
                    items = items.replace('Listing Stats','').strip()
                    items = items[-3:]
                    items = self.skuRef(items)
                    itemList.append(items)
                
            for items in etsy_orders[x]:
                if items.startswith('Height') or items.startswith('Size'):
                    items = items.replace('Height, # of Letters:' , '').strip()
                    items = items.replace('1-4', '').strip()
                    items = items.replace('5-6', '').strip()
                    items = items.replace('7-9', '').strip()
                    items = items.replace('10-12', '').strip()
                    items = items.replace('"', '').strip()
                    items = items.replace('Size:', '').strip()
                    items = items.replace('inches', '').strip()
                    items = items.replace('Inches', '').strip()
                    items = items.replace('Letters', '').strip()
                    items = items.replace('(most popular)', '').strip()
                    sizes.append(items)

            for items in etsy_orders[x]:
                if items.startswith('Note'):
                    items = items.replace('Note from Buyer', '').strip()
                    items = items.replace('Send Convo','')
                    items = items.replace(',','/').strip()
                    desc = items

            for items in etsy_orders[x]:
                if items.startswith("Shipping Method:"):
                    items = items.replace('Shipping Method: ', '').strip()
                    shipping = items
                    exp = ' '

            for items in etsy_orders[x]:
                if items.startswith('Color'):
                    items = items.replace('Unpainted','')
                    items = items.replace('Unpainted (goes out in 48 hrs)', '')
                    colors.append(items.replace('Color: ', '').strip())

            for items in etsy_orders[x]:
                if items.startswith('#'):
                    items = items.replace('#','')
                    dTime = time.strftime("%I:%M:%S")
                    daily = "etsy_orders/daily/etsy_ordernum_"+now+".txt"
                    with open(daily, 'a') as aFile:
                        aFile.write("%s\n" % items)
                    with open("etsy_orders/etsy_order_numbers.txt", "a") as output:
                        output.write("%s\n" % items)

            indices = [o for o, s in enumerate(etsy_orders[x]) if 'Quantity' in s]
            #print(indices)
            xyz=0
            while xyz < len(indices):
                quantloc.append(int(indices[xyz]) + 1)
                xyz+=1
            for i in quantloc:
                quant = re.sub('\,', ' ', etsy_orders[x][i].strip())
                quantityList.append(int(quant))
            try: 
                indices = [o for o, s in enumerate(etsy_orders[x]) if 'Ship To' in s]
                #print(indices)
                nameloc = int(indices[0]) + 1
                name = re.sub('\,', ' ', etsy_orders[x][nameloc].strip())
                shipname =  string.capwords(name)
            except IndexError:
                try:
                    shipname = re.sub('\,', ' ', etsy_orders[x][0].strip())
                except IndexError:
                    shipname = ''
            
            writter = 0
            try:
                while writter < len(itemList):   
                    suds = 0        
                    while suds < quantityList[writter]:
                        try:
                            size = sizes[writter]
                        except IndexError:
                            size = ''
                        try:
                            item = itemList[writter]
                        except IndexError:
                            item = ''
                        try:
                            color = colors[writter]
                        except IndexError:
                            color = ''
                        filewriter.writerow([purchaseDate, shop, size, item, color,'', desc, '', shipname, ' ', exp])
                        suds += 1
                    writter += 1
            except IndexError:
                break
            x += 1
        
    def writeAmznOrders(self, amord, filewriter):
        c = 0
        z = 0
        x = 0
        while c < len(amord):
            
            try:
                date = re.sub('\,', ' ', amord[c][7].strip())
                #purchaseDate = date.replace(date[:4], '').strip()
            except IndexError:
                date = ''
            try:    
                shop = re.sub('\,', ' ', amord[c][6].strip())
            except IndexError:
                shop = ' '
            xx = 0
            while xx < len(amord[c][5]):       
                try:
                    item = re.sub('\,', ' / ', amord[c][5][xx][1].strip())
                    item = item.replace('SKU: ', '').strip()
                    item = self.skuRef(item)
                    #item = item.replace('12-36 inch ','')
                except IndexError:
                    item = ' '             
                try:
                    size = re.sub('\,', ' ', amord[c][5][xx][7].strip())
                    size = size.replace('Size:', '').strip()
                    size = size.replace('Size and # of Letters:', '').strip() 
                    size = size.replace('inches (Most Popular)', '').strip()
                    size = size.replace('inches', '').strip()
                    size = size.replace('letters', '').strip()
                    size = size.replace('Inches', '').strip()
                    size = size.replace('Letters', '').strip()
                    size = size.replace('(single letter only)', '').strip()
                    size = size.replace('10-12', '').strip()
                    size = size.replace('5-6', '').strip()
                    size = size.replace('7-9', '').strip()
                    size = size.replace('1-4', '').strip()
                    size = size.replace('"', '').strip()
                except IndexError:
                    size = ' '
                try:
                    descript = re.sub('\,', ' ', amord[c][5][xx][8].strip())
                    descript = descript.replace('Name: ','').strip()
                    descript = descript.replace('Monogram Letter: ','').strip()
                    descript = descript.replace('Monogram Letters: ','').strip()
                    descript = descript.replace('Letters: ','').strip()
                    descript = descript.replace('Letter: ','').strip()
                    descript = descript.replace('Letter to be cut:','').strip()
                except IndexError:
                    descript = ' '
                try:   
                    name = re.sub('\,', ' ', amord[c][1][0].strip())
                except IndexError:
                    name = " "
                if amord[c][4] == "Shipping Service: Standard":
                    exp = ' '
                else:
                    exp = 'E'
                try:
                    quantity = amord[c][9]
                    #print (quantity)
                except IndexError:
                    quantity = '1'
                try:
                    if amord[c][5][xx][9][0] == "'":
                        try:
                            dot = re.sub('\,', ' ', amord[c][5][xx][9].strip())
                        except IndexError:
                            dot = ' '
                        connect = 0
                        if dot == "'i' or 'j' dot connected or unconnected: Shipped Loose" or dot == "'i or 'j' dot connected or unconnected?: Shipped Loose":
                            for a in descript:
                                #print (a)
                                if a =='j' or a == 'i':
                                    connect+=1
                            dot = 'Loose * '+str(connect)
                        else:
                            dot = ' '
                        try:
                            color = re.sub('\,', ' ', amord[c][5][xx][10].strip())
                            color = color.replace('Color: ','').strip()
                            if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                                color = ' '
                        except IndexError:
                            color = ' '
                    else:
                        dot = ' '
                        color = ' '
                except IndexError:
                    dot = ' '
                    try:
                        color = re.sub('\,', ' ', amord[c][5][xx][9].strip())
                        if color == 'Unpainted' or color == 'Unpainted (goes out in 48 hrs)':
                            color = ' '
                    except IndexError:
                        color = ' '
                xxx = 0
                try:
                    while xxx < int(quantity[xx]):
                        filewriter.writerow([date, shop, size, item, color,' ', descript, dot, name, ' ', exp])
                        xxx += 1
                except IndexError:
                    break
                xx += 1
            c+=1 


    def quitAll(self):
        #self.checkNewLoop('False')
        self.body.quit()
        sys.exit()

    def checkNewLoop(self,arg):
        if arg == 'True':
            t=threading.Timer(900.0,lambda: self.checkNewLoop('True'))
            t.daemon = True
            t.start()
            self.checkForNew()


    def checkForNew(self):
        self.status.set("Checking...")
        oldEtsy = []
        oldAm = []
        newAmOrders = 0
        newEtsyOrders = 0
        #newAm = self.pullAmOrderList()
        self.status.set("Checking for New Etsy Orders")
        with open("etsy_orders/etsy_order_numbers.txt", "r") as ordernumFile:
            for line in ordernumFile:
                oldEtsy.append(line.strip())

            #print(oldEtsy)
        newEtsy = self.pullEtsyOrderId()
        #print (newEtsy)
        for val in newEtsy:
            if val not in oldEtsy:
                newEtsyOrders+=1
        # print(newEtsyOrders)
        self.newEtsyOrd.set(self.newEtsyOrd.get()+newEtsyOrders)
        self.status.set("Checking for New Amazon Orders")
        with open("amzn_orders/amzn_order_numbers.txt", "r") as ordernumFile:
            for line in ordernumFile:
                oldAm.append(line.strip())
        newAm = self.pullAmOrderId()
        for val in newAm:
            if val not in oldAm:
                newAmOrders+=1
        # print(newAmOrders)
        self.newAmOrd.set(self.newAmOrd.get()+newAmOrders)
        newOrders = newAmOrders + newEtsyOrders
        dTime = time.strftime("%I:%M:%S")
        dateActual = time.strftime('%c')
        self.checktime.set(dateActual)
        oldcount = self.v.get()
        ordercount = oldcount + newOrders
        self.v.set(ordercount)
        self.status.set('ready')
        if newOrders > 0:
            self.startwriteCSV()
        return  

    def skuRef(self, sku):
        amznskuDict = {}
        etsyskuDict = {}
        with open('SKU_LIST/skuList.csv') as file:
            reader = csv.reader(file)
            for row in reader:
                etsyskuDict[row[1]] = row[0]
                amznskuDict[row[2]] = row[0]
                #print (row)
                #skuDict.update({col1:[col2, col3]})
        if sku in etsyskuDict:       
            return etsyskuDict.get(sku)
        if sku in amznskuDict:
            return amznskuDict.get(sku)
        else:
            return 'Custom Order'
        #print(etsyskuDict)
        #print(amznskuDict)

    def writeCSV(self):
        self.status.set("Pulling Data...")
        now = time.strftime("%m-%d-%Y")
        dateActual = time.strftime('%c')
        amazon_orders = self.pullAmOrderList()
        etsy_orders = self.pullEtOrderList()
        am_ord = self.getamznorderdataList(amazon_orders)
        with open('Daily_Spread/orders_'+now+'.csv', 'a') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
            self.writeEtsyOrders(etsy_orders, filewriter)
            self.writeAmznOrders(am_ord, filewriter)
        dTime = time.strftime("%H:%M:%S")
        dailyam = "amzn_orders/daily/amzn_ordernum_"+now+".txt"
        with open(dailyam, 'a') as aFile:
            for item in amazon_orders:
                aFile.write("%s\n" % item)

        with open("amzn_orders/amzn_order_numbers.txt", "a") as output:
            for item in amazon_orders:
                output.write("%s\n" % item)
        self.status.set('ready')
        return

    def createCSV(self):
        now = time.strftime("%m-%d-%Y")
        self.status.set("writting...")
        dateActual = time.strftime('%c')
        with open('Daily_Spread/orders_'+now+'.csv', 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
            filewriter.writerow(['Date', 'Shop', 'Size', 'Item', 'Color', 'Rack # (opt)', 'Notes', 'Connected/Count', 'Name', '#BOA', 'Express?', 'Weight',
                                'Box', 'Shipped Status', dateActual ])
        self.tt.set(dateActual)
        self.v.set(0)
        self.newEtsyOrd.set(0)
        self.newAmOrd.set(0)
        self.status.set('ready')
        return
            
''' def build(self):
        #create a button, and  attach animate() method as a on_press handler
        #layout = BoxLayout(padding=0)
        layout1 = BoxLayout(padding=10, orientation='vertical')
        layout = BoxLayout(padding = 10)
        button = Button(text="Search for new orders...")
        button.bind(on_press=self.checkForNew)
        button2 = Button(text='Create Spead Sheet')
        button2.bind(on_press=self.createCSV)
        layout.add_widget(button)
        layout.add_widget(button2)
        l = Label(text="48 Hour Monogram")
        Clock.schedule_interval(lambda x: self.update_label(x, new), 700)
        #x.bind(on_press=self.checkForNew)
        #x.text = new
        layout1.add_widget(l)
        new = self.checkForNew()
        x = Label(text=new)
        layout1.add_widget(x)

        layout1.add_widget(layout)

        #layout.add_widget(btn)
        return layout1'''

if __name__ == '__main__':

    info = AppKit.NSBundle.mainBundle().infoDictionary()
    info['LSUIElement'] = True
    app = FortyEightApp()
    



